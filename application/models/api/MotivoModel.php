<?php
require BASEPATH . '../vendor/predis/predis/autoload.php';
Predis\Autoloader::register();

class MotivoModel extends CI_Model {
	protected $redisConnection;
	protected $key = 'js:teste:motivos';

	public function __construct()
	{
		parent::__construct();
		$redisConfig = $this->config->item('redis');
		$this->redisConnection = new Predis\Client([
			'scheme' => 'tcp',
			'host' => $redisConfig['host'],
			'port' => $redisConfig['port'],
			'password' => $redisConfig['auth'],
		]);
	}

	public function lista()
	{
		return $this->redisConnection->hgetall($this->key);
	}
}
